=== Plugin Name ===
Contributors: Inpsyde
Donate link: inpsyde.com
Tags: ajax, json
Requires at least: 5.0
Tested up to: 5.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin collects data from 3rd party URL and display it on the website.

== Description ==
The plugin is created to fetch data from an external URL, display it on an arbitrary URL on the host website in the tabular form. When someone clicks on the cell of the table, a popup is open to display full detail of the cell. If there is an error fetching data from the external URL, the popup will show error code & message.

== Installation ==

1. Upload `inpsyde.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0 =
* Plugin just created.