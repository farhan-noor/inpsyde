<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       inpsyde.com
 * @since      1.0.0
 *
 * @package    Inpsyde
 * @subpackage Inpsyde/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Inpsyde
 * @subpackage Inpsyde/public
 * @author     Farhan Noor <info@inpsyde.com>
 */
class Inpsyde_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Inpsyde_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Inpsyde_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

                if( !$this->check_endpoint() ) return;
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/inpsyde-public.css', array(), $this->version, 'all' );
                wp_enqueue_style( $this->plugin_name.'-skeleton', plugin_dir_url( __FILE__ ) . 'css/skeleton.css', array(), $this->version, 'all' );
                wp_enqueue_style( $this->plugin_name.'-modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.2/jquery.modal.min.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Inpsyde_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Inpsyde_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
                if( !$this->check_endpoint() ) return;
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/inpsyde-public.js', array( 'jquery' ), $this->version, false );
                wp_enqueue_script( $this->plugin_name.'-modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.2/jquery.modal.min.js', array( 'jquery' ), $this->version, false );
	}
                
        function check_endpoint(){
            $page = sanitize_key(get_option('inp_endpoint', 'inp'));
            if( isset($_GET['endpoint']) AND $_GET['endpoint'] == $page ){
                return true;
            }
            else{
                return false;
            }
        }
                
        /**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
        function inpsyde_content($content){
            if( !$this->check_endpoint() ) return;
            ?>
            <!DOCTYPE html>
            <html <?php language_attributes(); ?>>
                <head>
                    <meta charset="<?php bloginfo( 'charset' ); ?>" />
                    <link rel="profile" href="http://gmpg.org/xfn/11" />
                    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
                    <title><?php esc_html_e('Inpsyde Table','inpsyde');?> </title>
                    <?php /*End of WP official headers*/ ?>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta name="robots" content="noindex,nofollow">
                    <?php wp_head(); ?>
                </head>
                <body>
                    <!-- Modal HTML -->
                    <div id="inpsyde-modal" class="modal">
                      <p></p>
                      <a href="#" rel="modal:close"><?php _e('Close', 'inpsyde'); ?></a>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="twelve columns">
                                <table class="<?php echo apply_filters('inpsyde_table_classes', 'inpsyde'); ?>" id="inpsyde_table">
                                    <thead>
                                        <tr><td><?php _e('ID', 'inpsyde'); ?></td><td><?php _e('Name', 'inpsyde'); ?></td><td><?php _e('Username', 'inpsyde'); ?></td></tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="3"><?php _e('Loading . . . .', 'inpsyde'); ?></td></tr>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                    <?php wp_footer(); ?>
                </body>
            </html>
            <?php
            exit;
        }
}
