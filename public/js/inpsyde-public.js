(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
        $(document).ready(function(){
            //Fill table on page load.
             var jqxhr = $.get('https://jsonplaceholder.typicode.com/users', function(data, status, xhr, ){
                   var output = null;
                   $.each(data, function(i,x){
                       output = output + '<tr><td>'+x.id+'</td><td>'+x.name+'</td><td>'+x.username+'</td></tr>';
                   });
                   $('#inpsyde_table tbody').html(output);
                   //$(test).appendTo('body').modal();
              });
              jqxhr.fail(function(xhr){
                  $('#inpsyde_table tbody tr td').html(xhr.status+': '+xhr.statusText).modal();
              });
            
           //Show user detailed info in the popup when table element is clicked
           $('.inpsyde td a').click(function(event){
               event.preventDefault();
               this.blur(); // Manually remove focus from clicked link.
               
               $('#inpsyde-modal').text('Loading . . . .').modal();
               
               var jqxhr = $.get(this.href, function(data, status, xhr, ){
                   var output = '<ul>';
                   $.each(data, function(i,x){
                       if(Array.isArray(x)) output = output + '<li>Hello World</li>';
                       output = output + '<li>'+i+': '+x+'</li>';
                   });
                   output = output + '</ol>';
                   $('#inpsyde-modal').html(output).modal();
                   //$(test).appendTo('body').modal();
              });
              jqxhr.fail(function(xhr){
                  $('#inpsyde-modal').html(xhr.status+': '+xhr.statusText).modal();
              });
           });
        });

})( jQuery );
