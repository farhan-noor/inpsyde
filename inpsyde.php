<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              inpsyde.com
 * @since             1.0.0
 * @package           Inpsyde
 *
 * @wordpress-plugin
 * Plugin Name:       Inp Syde
 * Plugin URI:        inpsyde.com
 * Description:       Inpsyde skill test plugin.
 * Version:           1.0.0
 * Author:            Farhan Noor
 * Author URI:        https://inpsyde.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       inpsyde
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'INPSYDE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-inpsyde-activator.php
 */
function activate_inpsyde() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-inpsyde-activator.php';
	Inpsyde_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-inpsyde-deactivator.php
 */
function deactivate_inpsyde() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-inpsyde-deactivator.php';
	Inpsyde_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_inpsyde' );
register_deactivation_hook( __FILE__, 'deactivate_inpsyde' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-inpsyde.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_inpsyde() {

	$plugin = new Inpsyde();
	$plugin->run();

}
run_inpsyde();
