<?php

/**
 * Fired during plugin deactivation
 *
 * @link       inpsyde.com
 * @since      1.0.0
 *
 * @package    Inpsyde
 * @subpackage Inpsyde/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Inpsyde
 * @subpackage Inpsyde/includes
 * @author     Farhan Noor <info@inpsyde.com>
 */
class Inpsyde_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
