<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       inpsyde.com
 * @since      1.0.0
 *
 * @package    Inpsyde
 * @subpackage Inpsyde/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Inpsyde
 * @subpackage Inpsyde/admin
 * @author     Farhan Noor <info@inpsyde.com>
 */
class Inpsyde_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Inpsyde_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Inpsyde_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/inpsyde-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Inpsyde_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Inpsyde_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/inpsyde-admin.js', array( 'jquery' ), $this->version, false );

	}
        
        public function add_submenu_page(){
            add_options_page( 'INPSYDE options', 'Inpsyde', 'manage_options', 'Inpsyde', array($this, 'options_page_output') );
        }

        public function options_page_output(){
            ?>
            <div class="wrap">
            <h1>Inpsyde Settings</h1>

            <form method="post" action="options.php" novalidate="novalidate">
            <input type="hidden" name="option_page" value="general"><input type="hidden" name="action" value="update"><input type="hidden" id="_wpnonce" name="_wpnonce" value="9e66efa2ae"><input type="hidden" name="_wp_http_referer" value="/wordpress/wp-admin/options-general.php">
            <table class="form-table" role="presentation">

            <tbody><tr>
            <th scope="row"><label for="blogname">Site Title</label></th>
            <td><input name="blogname" type="text" id="blogname" value="WordPress" class="regular-text"></td>
            </tr>
            </tbody></table>


            <p class="submit"><?php submit_button(); ?></form>

            </div>
            <?php
        }
}
